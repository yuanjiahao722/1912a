import { Button, message } from "ant-design-vue"
import { App } from "vue"
const install = (app: App): void => {
  app.use(Button)
  app.config.globalProperties.$message = message
}

export default {
  install,
}
