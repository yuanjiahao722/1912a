const path = require("path")

// const { defineConfig } = require("@vue/cli-service")

// module.exports = defineConfig({
//   transpileDependencies: true,
// })

module.exports = {
  lintOnSave:false,
  chainWebpack: (config) => {
    const types = ["vue-modules", "vue", "normal-modules", "normal"]
    types.forEach((type) =>
      addStyleResource(config.module.rule("less").oneOf(type))
    )
  },
  css: {
    loaderOptions: {
      less: {
        lessOptions: {
          javascriptEnabled: true,
        },
      },
    },
  },
  configureWebpack: {
    resolve: {
      alias: {
        views: path.join(__dirname, "src/views"),
      },
    },
  },
}

function addStyleResource(rule) {
  rule
    .use("style-resource")
    .loader("style-resources-loader")
    .options({
      patterns: [path.resolve(__dirname, "./src/styles/var.less")],
    })
}
